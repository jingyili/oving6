#include "utility.hpp"
#include <cstring>
#include <iostream>

int main() {
  char *city = "Trondheim";
  std::cout << is_capital_of_norway(city, strlen(city)) << std::endl;

  city = "Oslo";
  std::cout << is_capital_of_norway(city, strlen(city)) << std::endl;
}
