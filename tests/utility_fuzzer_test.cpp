#include "utility.hpp"
#include <string>

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
  is_capital_of_norway(reinterpret_cast<const char *>(data), size);
  return 0;
}
