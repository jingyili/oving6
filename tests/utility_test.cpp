#include "assert.hpp"
#include "utility.hpp"
#include <cstring>

int main() {
  char *city = "";
  ASSERT(is_capital_of_norway(city, strlen(city)) == false);

  city = "Trondheim";
  ASSERT(is_capital_of_norway(city, strlen(city)) == false);

  city = "Oslo";
  ASSERT(is_capital_of_norway(city, strlen(city)) == true);
}
